﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueControl : MonoBehaviour
{
    public Animator jugadorAnim;
    
    public GameObject colliderAtaque; 

    void Start()
    {
        
    }

    
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            jugadorAnim.SetTrigger("golpeo");
        }
    }

    public void Ataque()
    {
        colliderAtaque.SetActive(true);
    }

    public void NoAtaque()
    {
        colliderAtaque.SetActive(false);
    }
}
