﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemigoControl : MonoBehaviour
{
    public int hp;
    public int armaDaño;
    public GameObject explosion;
    private GameObject jugador;
    public int rapidez;
     NavMeshAgent nav;
    

    void Start()
    {
        jugador = GameObject.Find("Jugador");
        nav = GetComponent<NavMeshAgent>();
    }

    
    void Update()
    {
        nav.SetDestination(jugador.transform.position);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "armaImpacto")
        {
            hp -= armaDaño;
        }

        if (hp <= 0)
        {
            Invoke("MostrarExplosion",1);
            Destroy(gameObject,1);
        }
        
    }

    public void MostrarExplosion()
    {
        GameObject particulas = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 2);

    }
}
