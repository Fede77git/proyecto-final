﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaAgarre : MonoBehaviour
{
    public GameObject Jugador;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Jugador)
        {
            Jugador.transform.parent = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Jugador)
        {
            Jugador.transform.parent = null;
        }
    }
}
