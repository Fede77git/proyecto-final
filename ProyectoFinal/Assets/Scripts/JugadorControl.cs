﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JugadorControl : MonoBehaviour
{
    

    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200.0f;
    private Animator anim;
    public float x, y;

    public Rigidbody rb;
    public float fuerzaDeSalto = 10f;
    public bool puedoSaltar;
    public Text textoGanaste;
    public Text textoCantidadRecolectados;
    
    private int cont;

    Vector3 posInicial;


    void Start()
    {
        puedoSaltar = false;
        anim = GetComponent<Animator>();
        Cursor.lockState = CursorLockMode.Locked;
        cont = 0;
        setearTextos();
        posInicial = this.transform.position;
        GestorDeAudio.instancia.ReproducirSonido("musica");
    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Collected masks: " + cont.ToString();
        if (cont >= 6)
        {
            textoGanaste.text = "Thanks for playing";
            Time.timeScale = 0.2f;
            SceneManager.LoadScene("menuFinal");
            Cursor.lockState = CursorLockMode.None;
        }
    }


    void FixedUpdate()
    {
        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);
    }

   
    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }


        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        
        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);

        if (puedoSaltar == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                anim.SetBool("Salto", true);
                rb.AddForce(new Vector3(0, fuerzaDeSalto, 0), ForceMode.Impulse);
            }
            anim.SetBool("TocandoSuelo", true);
        }
        else
        {
            MeCaigo();
        }
    }

    public void MeCaigo()
    {
        
        anim.SetBool("TocandoSuelo", false);
        anim.SetBool("Salto", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Recolectable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }
    }

    public void ResetPosition()
    {
        this.transform.position = posInicial;
    }

    
}
