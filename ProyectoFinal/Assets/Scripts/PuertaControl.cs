﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaControl : MonoBehaviour
{
    public float speed;
    public float angle;
    public Vector3 direction;
    public bool puedoAbrir;

    void Start()
    {
        angle = transform.eulerAngles.y;
    }

   
    void Update()
    {
        if (Mathf.Round(transform.eulerAngles.y)!= angle)
        {
            transform.Rotate(direction * speed);
        }
        if (Input.GetKeyDown("e") && puedoAbrir == true)
        {
            angle = 80;
            direction = Vector3.up;
        }


    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Jugador")
        {
            puedoAbrir = true;
        }
         
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Jugador")
        {
            puedoAbrir = false;
        }

    }
}
