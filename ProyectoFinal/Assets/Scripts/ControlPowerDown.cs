﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPowerDown : MonoBehaviour
{
    public JugadorControl jugadorControl;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerDown") == true)
        {
            jugadorControl.fuerzaDeSalto = 3f;
            other.gameObject.SetActive(false);
        }
    }
}
