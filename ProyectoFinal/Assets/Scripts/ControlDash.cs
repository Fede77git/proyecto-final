﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDash : MonoBehaviour
{

    JugadorControl jugadorDash;
    public float dashSpeed;
    public float dashTime;
    public float dashCD;

    void Start()
    {
        jugadorDash = GetComponent<JugadorControl>();

    }

    // Update is called once per frame
    void Update()
    {
        dashCD -= Time.deltaTime;
        if (Input.GetButtonDown("Fire3"))
        {
            if (dashCD <= 0)
            {
                StartCoroutine(Dash());
            }
        }

    }

    IEnumerator Dash()
    {
        float startTime = Time.time;

        while (Time.time < startTime + dashTime)
        {
            transform.Translate(Vector3.forward * dashSpeed);
            dashCD = 3;

            yield return null;
        }
    }
}