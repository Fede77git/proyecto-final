﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMuerte : MonoBehaviour
{
    public JugadorControl jugadorControl;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            jugadorControl.ResetPosition();
        }
    }
}
