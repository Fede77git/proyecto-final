﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBossFinal : MonoBehaviour
{
    public int hp;
    public int armaDaño;
    public GameObject explosion;
    private GameObject jugador;
    public GameObject Recolectable;
    


    void Start()
    {
        
        
    }


    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "armaImpacto")
        {
            hp -= armaDaño;
        }

        if (hp <= 0)
        {
            Invoke("MostrarExplosion", 1);
            Destroy(gameObject, 1);
            DropItem();
        }

    }

    public void MostrarExplosion()
    {
        GameObject particulas = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 3);

    }

    private void DropItem()
    {
        Vector3 position = transform.position;
        GameObject recolectable = Instantiate(Recolectable, position + new Vector3(0.0f, 1.0f, 0.0f), Quaternion.identity);
        recolectable.SetActive(true);
        Destroy(recolectable, 5);
    }
}
