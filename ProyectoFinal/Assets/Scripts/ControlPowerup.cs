﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPowerup : MonoBehaviour
{
    public JugadorControl jugadorControl;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUpSalto") == true)
        {
            jugadorControl.fuerzaDeSalto = 8f;
            other.gameObject.SetActive(false);
        }
    }
}
